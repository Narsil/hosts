## About

Several privacy hosts in order to improve Privacy.-

## Credits
Thanks to:
- [crazy-max](https://github.com/crazy-max) for its Windows hosts.
- [Steven Black](https://github.com/StevenBlack/hosts) for their complete hosts.
- [Perflyst](https://github.com/Perflyst) for its Smart TV hosts.
- [jjjxu](https://github.com/jjjxu/NSO_Pegasus_Blocklist) for its Pegasus Blocklist.
- [blokada exodus privacy host](https://blokada.org/mirror/v5/exodusprivacy/standard/hosts.txt)
- [gallegonovato](https://git.nixnet.xyz/gallegonovato) - For his suggestions and support.

## License
GNU General Public License v3.0.

See `LICENSE` for more details.
