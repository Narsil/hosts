## About

This is a fork from [crazy-max](https://github.com/crazy-max) project.
Add them to your host list.

SpyMicroft is spy host list and Microsoft is extra and update hosts:
[crazy-max Windows hosts](https://github.com/crazy-max/WindowsSpyBlocker/tree/master/data/hosts)

I have added other hosts I've found using Wireshark and tcpdump as well as some common hosts we could see using Windows (Firefox, Nvidia and so on)

The purpose is zero automatic connections using Windows.

Anyway, SmartScreen may send some information even we add those connections to our host list (go.microsoft.com mainly)
Due to this, I strongly recommend adding them to a LibreCMC/OpenWRT router with adblock or using PiHole.


Finally, install opensource Simplewall firewall and deny any connection to Microsoft.
