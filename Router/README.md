## About

Personal hosts to avoid using a LibreCMC router and ublock origin with Firefox.

Samsung host list is provided by [Perflyst](https://github.com/Perflyst)

NSA lists were provided by [CHEF-KOCH](https://github.com/CHEF-KOCH/NSABlocklist)