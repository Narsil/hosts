## Improving privacy using Tor Browser

Editing torrc in order to enhance privacy for this browser.

* Excluding [Five Eyes nodes](https://en.wikipedia.org/wiki/Five_Eyes)

_ExcludeExitNodes {us},{au},{ca},{nz},{gb}_

_ExcludeNodes {us},{au},{ca},{nz},{gb}_

_StrictNodes 1_


* By adding MapAddress hosts with the purpose of blocking ads. This host is mainly based in Steven Black hosts.

_MapAdress domain.com 0.0.0.0_


## Considerations

As for NoGoogle hosts, *.googlevideo.com is necessary for Invidious. Modify or delete it if you want.

By using torrc, by default, most of GAFAM hosts will be blocked due to this is a personal list.
In the past, I've used a Steven Black one but doesn't work now.

## Credits
Thanks to:
- [Baobab codeberg](https://codeberg.org/baobab) for his MapAddress idea.
- [Steven Black](https://github.com/StevenBlack/hosts) for his hosts.
- [nickspaargaren](https://github.com/nickspaargaren/no-google) for NoGoogle hosts.


## License
GNU General Public License v3.0.

See `LICENSE` for more details.
