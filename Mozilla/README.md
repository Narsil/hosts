## About

Blocking automatic connections using Firefox Desktop and for Android.

However, addons will not update so use it at your own risk.

## Credits
Thanks to:
- [MrRawes](https://github.com/MrRawes/firefox-hosts) for its Firefox hosts.